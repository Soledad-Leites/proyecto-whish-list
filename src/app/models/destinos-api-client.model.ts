import { DestinoViaje } from './destino-viaje.models';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-viajes-state.model';
import { Injectable } from '@angular/core';

@Injectable()
export class DestinosApiClient {
  constructor(private store: Store<AppState>) {
  }
  add(x: DestinoViaje) {
    this.store.dispatch(new NuevoDestinoAction(x));
  }
  elegir(x: DestinoViaje) {
    this.store.dispatch(new ElegidoFavoritoAction(x));
  }
}